<?php

use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = Yii::$app->name;
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>
            <?= Yii::t('app', 'Курс валют') ?>
        </h1>

        <p class="lead">
            <?= Yii::t('app', 'Тестовое задание для CreditExpress') ?>
        </p>

        <p style="margin-top: 100px;">
            <a class="btn btn-lg btn-success" href="<?= Url::to(['/cabinet']) ?>">
                <?= Yii::t('app', 'Перейти') ?>
            </a>
        </p>
    </div>
</div>
