<?php
$o_user = Yii::$app->user->getIdentity();
?>

<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="ru">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">

        <title><?= $this->title ?></title>

        <!-- Fevicon -->
        <link rel="shortcut icon" href="/favicon.ico">

        <!-- Start CSS -->
        <!-- Chartist Chart CSS -->
        <link rel="stylesheet" href="/themes/xs/plugins/chartist-js/chartist.min.css">

        <!-- Datepicker CSS -->
        <!-- Нужно делать через Assets, но это долго :) -->
        <link href="/themes/xs/plugins/datepicker/datepicker.min.css" rel="stylesheet" type="text/css">
        <link href="/themes/aplomb/plugins/alertify/css/alertify.css" rel="stylesheet" type="text/css">

        <link href="/themes/xs/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="/themes/xs/css/icons.css" rel="stylesheet" type="text/css">
        <link href="/themes/xs/css/style.css?4" rel="stylesheet" type="text/css">
        <link href="/themes/aplomb/plugins/sweet-alert2/sweetalert2.css" rel="stylesheet" type="text/css">

        <link href="/themes/aplomb/plugins/datatables/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css"
              xmlns="http://www.w3.org/1999/html"/>
        <link href="/themes/aplomb/plugins/datatables/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
        <!-- Responsive datatable examples -->
        <link href="/themes/aplomb/plugins/datatables/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css"/>
        <!-- End CSS -->

        <?php $this->head() ?>
    </head>

    <body class="xp-vertical">
    <?php $this->beginBody() ?>
    <!-- Search Modal -->

    <!-- Start XP Container -->
    <div id="xp-container">

        <!-- Start XP Leftbar -->
        <div class="xp-leftbar">
            <!-- Start XP Sidebar -->
            <div class="xp-sidebar">

                <!-- Start XP Logobar -->
                <div class="xp-logobar text-center">
                    <a href="/" class="xp-logo">
                        <?= Yii::$app->name ?>
                    </a>
                </div>
                <!-- End XP Logobar -->

                <!-- Start XP Navigationbar -->
                <div class="xp-navigationbar">
                    <ul class="xp-vertical-menu">
                        <li <?= preg_match('/cabinet/', Yii::$app->request->url) ? 'class="active"' : '' ?>>
                            <a href="<?= Yii::$app->urlManager->createUrl(['/cabinet']) ?>">
                                <i class="mdi mdi-currency-usd"></i>
                                <span><?= Yii::t('app', 'Курс валют') ?></span>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- End XP Navigationbar -->

            </div>
            <!-- End XP Sidebar -->

        </div>
        <!-- End XP Leftbar -->

        <!-- Start XP Rightbar -->
        <div class="xp-rightbar">

            <!-- Start XP Topbar -->
            <div class="xp-topbar">

                <!-- Start XP Row -->
                <div class="row">

                    <!-- Start XP Col -->
                    <div class="col-2 col-md-1 col-lg-1 order-2 order-md-1 align-self-center">
                        <div class="xp-menubar">
                            <a class="xp-menu-hamburger" href="javascript:void();">
                                <i class="icon-menu font-20 text-white"></i>
                            </a>
                        </div>
                    </div>
                    <!-- End XP Col -->

                    <!-- Start XP Col -->
                    <div class="col-10 col-md-11 col-lg-11 order-1 order-md-2">
                        <div class="xp-profilebar text-right">
                            <ul class="list-inline mb-0">
                                <li class="list-inline-item mr-0">
                                    <div class="dropdown xp-userprofile">
                                        <a class="dropdown-toggle" href="javascript:void(0)" role="button"
                                           id="xp-userprofile"
                                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                            <i class="icon-user" style="color:white;"></i>

                                            <span class="xp-user-live"></span></a>

                                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="xp-userprofile">
                                            <a class="dropdown-item py-3 text-white text-center font-16"
                                               href="javascript:void(0)">
                                                <?= $o_user->username ?>
                                            </a>

                                            <a class="dropdown-item"
                                               href="<?= Yii::$app->urlManager->createUrl(['/site/logout']) ?>">
                                                <i class="icon-power text-danger mr-2"></i>
                                                <?= Yii::t('app', 'Выйти') ?>
                                            </a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- End XP Col -->

                </div>
                <!-- End XP Row -->

            </div>
            <!-- End XP Topbar -->

            <!-- Start XP Breadcrumbbar -->
            <div class="xp-breadcrumbbar">
                <div class="row">
                    <div class="col-md-6 col-lg-6">
                        <h4 class="xp-page-title"><?= $this->title ?></h4>
                    </div>
                    <div class="col-md-6 col-lg-6">
                        <div class="xp-breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="/"><i class="icon-home"></i></a></li>
                                <?php if (Yii::$app->request->url != '/cabinet') : ?>
                                    <li class="breadcrumb-item">
                                        <a href="/cabinet"><?= Yii::t('app', 'Главная') ?></a>
                                    </li>
                                <?php endif; ?>
                                <li class="breadcrumb-item" aria-current="page">
                                    <?= $this->title ?>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End XP Breadcrumbbar -->

            <!-- Start XP Contentbar -->
            <div class="xp-contentbar">
                <?= $content ?>
            </div>
            <!-- End XP Contentbar -->

            <!-- Start XP Footerbar -->
            <div class="xp-footerbar">
                <footer class="footer">
                    <p class="mb-0">© <?= date('Y') ?> <?= Yii::$app->name ?>
                        - <?= Yii::t('app', 'Все права защищены') ?>.</p>
                </footer>
            </div>
            <!-- End XP Footerbar -->

        </div>
        <!-- End XP Rightbar -->

    </div>
    <!-- End XP Container -->

    <!-- Start JS -->
    <script src="/themes/xs/js/jquery.min.js"></script>
    <script src="/themes/xs/js/popper.min.js"></script>
    <script src="/themes/xs/js/bootstrap.min.js"></script>
    <script src="/themes/aplomb/plugins/sweet-alert2/sweetalert2.min.js"></script>
    <script src="/themes/aplomb/plugins/alertify/js/alertify.js"></script>
    <script src="/themes/xs/js/modernizr.min.js"></script>
    <script src="/themes/xs/js/detect.js"></script>
    <script src="/themes/xs/js/jquery.slimscroll.js"></script>
    <script src="/themes/xs/js/sidebar-menu.js"></script>

    <script src="/themes/xs/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="/themes/xs/plugins/datatables/dataTables.bootstrap4.min.js"></script>
    <script src="/themes/xs/js/init/table-datatable-init.js"></script>
    <script src="/themes/xs/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="/themes/xs/plugins/datatables/dataTables.buttons.min.js"></script>

    <!-- Chartist Chart JS -->
    <script src="/themes/xs/plugins/chartist-js/chartist.min.js"></script>
    <script src="/themes/xs/plugins/chartist-js/chartist-plugin-tooltip.min.js"></script>
<!--    <script src="/themes/xs/js/init/chartistjs-init.js"></script>-->

    <!-- Datepicker JS -->
    <script src="/themes/xs/plugins/datepicker/datepicker.min.js"></script>
    <script src="/themes/xs/plugins/datepicker/i18n/datepicker.en.js"></script>

    <!-- Main JS -->
    <script src="/themes/xs/js/main.js"></script>
    <!-- End JS -->

    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>