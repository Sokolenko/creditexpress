<?php

use yii\helpers\Html;
use yii\widgets\Pjax;

/** @var $a_label array */
/** @var $a_series array */
/** @var $a_currency array */
/** @var $period int */

$this->title = Yii::t('app', 'Курс валют');
?>

<?php Pjax::begin(); ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="card m-b-30">
                <div class="card-header bg-white">
                    <h5 class="card-title text-black">
                        <?= Yii::t('app', 'Курс валют') ?>
                    </h5>
                    <h6 class="card-subtitle">
                        <?= Yii::t('app', 'Показатели за последние 10 дней') ?>
                    </h6>
                </div>
                <div class="card-body">
                    <div class="xp-chart-label">
                        <ul class="list-inline">
                            <?php
                            $a_color = ['primary', 'success', 'danger', 'warning'];
                            ?>
                            <?php foreach ($a_currency as $i => $name) : ?>
                                <li class="list-inline-item">
                                    <p>
                                        <i class="mdi mdi-circle-outline text-<?= $a_color[$i] ?>"></i>
                                        <?= $name ?>
                                    </p>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <div class="col-sm-4 offset-4">
                        <?= Html::dropDownList('period', $period, [
                            7 => Yii::t('app', 'Неделя'),
                            10 => Yii::t('app', '10 дней'),
                            14 => Yii::t('app', '2 недели'),
                        ], [
                            'id' => 'period',
                            'class' => 'xp-select2-single form-control'
                        ]) ?>
                    </div>
                    <div id="xp-chartist-only-holes-in-data"
                         class="ct-chart ct-golden-section xp-chartist-only-holes-in-data"></div>
                </div>
            </div>
        </div>
    </div><!-- end row -->
<?php Pjax::end() ?>

<?php
$labels = json_encode($a_label);
$series = json_encode($a_series);

$js = <<<JS
    $('#period').change(function() {
        t = $(this);
        period = t.val();
        window.location.search = '?period=' + period;   
    });
 
    function xpChartistOnlyHoleInData() {
        new Chartist.Line('#xp-chartist-only-holes-in-data', {
            labels: $labels,
            series: $series
        }, {
            high: 100,
            low: 10,
            fullWidth: false,
            axisY: {
                onlyInteger: true,
                offset: 20
            },
            plugins: [
                Chartist.plugins.tooltip()
            ]
        });
    }
    
    xpChartistOnlyHoleInData();
JS;

$this->registerJs($js, \yii\web\View::POS_END);
?>