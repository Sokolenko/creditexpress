<?php

namespace app\modules\cabinet\controllers;

use app\models\CurrencyForm;
use Yii;
use yii\web\Controller;

class SiteController extends Controller
{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $o_request = Yii::$app->request;
        $o_CbRF = Yii::$app->CbRF;

        $period = (int)$o_request->get('period', 7);

        $date_from = time() - 3600 * 24 * $period;
        $date_to = date('d.m.Y'); // Today
        $a_CbRF = $o_CbRF
            ->filter(['currency' => $o_CbRF->defaultCurrency])
            ->withDynamic([
                'date_from' => date('d.m.Y', $date_from),
                'date_to' => $date_to])
            ->all();

        $a_label = $a_series = [];

        foreach ($a_CbRF as $a_currency) {
            if (!$a_label)
                $is_label = !0;

            $a_lines = [];
            foreach ($a_currency['dynamic'] as $a_dynamic) {
                $a_lines[] = $a_dynamic['value'];

                if ($is_label)
                    $a_label[] = date('d.m.Y', $a_dynamic['date']);
            }
            $a_series[] = $a_lines;
            $is_label = !1;
        }

        $a_currency = array_keys($a_CbRF);

        return $this->render('index', [
            'a_label' => $a_label,
            'a_series' => $a_series,
            'a_currency' => $a_currency,
            'period' => $period
        ]);
    }

}
