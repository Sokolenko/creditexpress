
/*
 Template Name: Aplomb - Bootstrap 4 Admin Dashboard
 Author: Mannatthemes
 Website: www.mannatthemes.com
 File: Dashboard-2 init js
 */

!function($) {
    "use strict";

    var Dashboard = function() {};

    

     //creates Bar chart
     Dashboard.prototype.createBarChart  = function(element, data, xkey, ykeys, labels, lineColors) {
        Morris.Bar({
            element: element,
            data: data,
            xkey: xkey,
            ykeys: ykeys,
            labels: labels,
            gridLineColor: '#444444',
            barSizeRatio: 0.4,
            resize: true,
            hideHover: 'auto',
            barColors: lineColors,
            fillOpacity: 0.1,
            grid: false,
            axes: false,
            barRadius: [5, 5, 0, 0],            
        });
    },

    
      //creates area chart
      Dashboard.prototype.createAreaChart = function(element, pointSize, lineWidth, data, xkey, ykeys, labels, lineColors) {
        Morris.Area({
            element: element,
            pointSize: 3,
            lineWidth: 2,
            data: data,
            xkey: xkey,
            ykeys: ykeys,
            labels: labels,
            resize: true,
            hideHover: 'auto',
            gridLineColor: '#bf6565',
            lineColors: lineColors,
            fillOpacity: 0.6,
            xLabelMargin: 10,
            yLabelMargin: 10,
            grid: false,
            axes: false,
            pointSize: 0
        });
    },

     //creates Donut chart
     Dashboard.prototype.createDonutChart = function(element, data, colors) {
        Morris.Donut({
            element: element,
            data: data,
            resize: true,
            colors: colors,
            labelColor: '#ccc',
            backgroundColor: 'transparent',
            fillOpacity: 0.1,
            //formatter: function (x) { return x + "%"}
        });
       
    },

  

    //nice scroll
    $(".boxscroll").niceScroll({cursorborder:"#6b7267",cursorcolor:"#6b7267",boxzoom:true});
    $( ".data-attributes span" ).peity( "donut" );


    
    Dashboard.prototype.init = function() {
          
            //creating bar chart
            var $barData = [
             {y: 'January', a: 20, b: 30},
             {y: 'February', a: 40, b: 50},
             {y: 'March', a: 50, b: 40},
             {y: 'April', a: 60, b: 70},
             {y: 'May', a: 50, b: 40},
             {y: 'June', a: 75, b: 65},
             {y: 'July', a: 80, b: 90},
             {y: 'August', a: 90, b: 75}
         ];
         this.createBarChart('morris-bar-example', $barData, 'y', ['a', 'b'], ['$', 'Sales'], ['#6dafc3 ', '#7ca2a9']);

           //creating area chart
           var $areaData = [
            {y: '2011', a: 10, b: 15},
            {y: '2012', a: 30, b: 35},
            {y: '2013', a: 10, b: 25},
            {y: '2014', a: 55, b: 45},
            {y: '2015', a: 30, b: 20},
            {y: '2016', a: 40, b: 35},
            {y: '2017', a: 10, b: 25},
            {y: '2018', a: 25, b: 30}
        ];
        this.createAreaChart('morris-area-chart', 0, 0, $areaData, 'y', ['a', 'b'], ['inbound A', 'outbound B'], ['#8096a6', '#6dafc3']);

         //creating donut chart
         var $donutData = [
            {label: "New York", value: 200},
            {label: "Canada", value: 615},
            {label: "Japan", value: 320}, 
        ];
        
        this.createDonutChart('morris-donut-example', $donutData, ['rgba(211, 218, 232,0.4)','rgba(118, 205, 222,0.4)', 'rgba(255, 255, 255,0.5)']);
       
    },
    //init
    $.Dashboard = new Dashboard, $.Dashboard.Constructor = Dashboard
}(window.jQuery),

//initializing 
function($) {
    "use strict";
    $.Dashboard.init();
}(window.jQuery);